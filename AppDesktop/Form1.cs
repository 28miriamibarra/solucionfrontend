﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AppDesktop
{
    public partial class Form1 : Form
    {
        
        
        public Form1()
        {
            InitializeComponent();
        }

        private void txtSaldo_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            int Folio;
            double Saldo;
            string Cliente;
            try
            {
                var WS = new Servicio.ServicioWeb();
                //Folio = int.Parse(txtFolio.Text);
                Cliente = txtCliente.Text;
                Saldo = double.Parse(txtSaldo.Text);
                if (txtFolio.Text == "" )
                {
                    if (WS.Guardar(Cliente, Saldo))
                    {
                        lblStatus.Text = "Guardado Correctamente";
                        txtCliente.Text = "";
                        txtSaldo.Text = "";
                        txtFolio.Text = "";
                    }
                    else
                    {
                        lblStatus.Text = "No guardado";
                    }

                }
                else
                {
                    Folio = int.Parse(txtFolio.Text);
                    if (WS.Actualizar(Folio, Cliente, Saldo))
                    {
                        lblStatus.Text = "Actualizado Correctamente";
                        txtCliente.Text = "";
                        txtSaldo.Text = "";
                        txtFolio.Text = "";
                    }
                    else
                    {
                        lblStatus.Text = "No se actualizo el registro";
                    }



                }
            }
            catch (Exception ex)
            {

                lblStatus.Text = ex.Message;
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            lblStatus.Text = "Status";
            int Folio;
            var Conjunto = new DataSet();
            DataRow Renglon;
            try
            {
                
                Folio = int.Parse(txtFolio.Text);
                var WS = new Servicio.ServicioWeb();
                Conjunto = WS.BuscarRegistro(Folio);
                Renglon = Conjunto.Tables["Datos"].Rows[0];
                txtCliente.Text = Renglon["Cliente"].ToString();
                txtSaldo.Text = Renglon["Saldo"].ToString();
                
            }
            catch (Exception ex)
            {
                lblStatus.Text = "No existe el registro";
                txtCliente.Text = "";
                txtSaldo.Text = "";
                //lblStatus.Text = ex.Message;

            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            int Folio;
            try
            {
                var WS = new Servicio.ServicioWeb();
                Folio = Convert.ToInt32(txtFolio.Text);
                if (WS.Eliminar(Folio))
                {
                    lblStatus.Text = "Se ha eliminado el registro";
                    txtCliente.Text = "";
                    txtSaldo.Text = "";
                    txtFolio.Text = "";
                }
                else
                {
                    lblStatus.Text = "No se elimino el registro";
                }
            }
            catch (Exception ex)
            {
                lblStatus.Text = ex.Message;
            }
        }
    }
}
