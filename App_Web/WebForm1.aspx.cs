﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace App_Web
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void txtFolio_TextChanged(object sender, EventArgs e)
        {

        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            int Folio;
            double Saldo;
            string Cliente;
            try
            {
                var WS = new Servicio.ServicioWeb();
                //Folio = int.Parse(txtFolio.Text);
                Cliente = txtCliente.Text;
                Saldo = double.Parse(txtSaldo.Text);
                if (txtFolio.Text == "")
                {
                    if (WS.Guardar(Cliente, Saldo))
                    {
                        lblStatus.Text = "Guardado Correctamente";
                        txtCliente.Text = "";
                        txtSaldo.Text = "";
                        txtFolio.Text = "";
                    }
                    else
                    {
                        lblStatus.Text = "No guardado";
                    }
                }
                else
                {
                    Folio = int.Parse(txtFolio.Text);
                    if (WS.Actualizar(Folio, Cliente, Saldo))
                    {
                        lblStatus.Text = "Actualizado Correctamente";
                        txtCliente.Text = "";
                        txtSaldo.Text = "";
                        txtFolio.Text = "";
                    }
                    else
                    {
                        lblStatus.Text = "No se actualizo el registro";
                    }
                }
            }
            catch (Exception ex)
            {

                lblStatus.Text = ex.Message;
            }


            /*double Saldo;
            string Cliente;
            try
            {
                var WS = new Servicio.ServicioWeb();
                Cliente = txtCliente.Text;
                Saldo = double.Parse(txtSaldo.Text);
                if (WS.Guardar(Cliente, Saldo))
                {
                    lblStatus.Text = "Guardado Correctamente";
                }
                else
                {
                    lblStatus.Text = "No guardado";
                }
            }
            catch (Exception ex)
            {

                lblStatus.Text = ex.Message;
            }*/
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            int Folio;
            var Conjunto = new DataSet();
            DataRow Renglon;
            try
            {
                lblStatus.Text = "";
                Folio = int.Parse(txtFolio.Text);
                var WS = new Servicio.ServicioWeb();
                Conjunto = WS.BuscarRegistro(Folio);
                Renglon = Conjunto.Tables["Datos"].Rows[0];
                txtCliente.Text = Renglon["Cliente"].ToString();
                txtSaldo.Text = Renglon["Saldo"].ToString();

            }
            catch (Exception ex)
            {
                //lblStatus.Text = ex.Message;
                lblStatus.Text = "No existe el registro";
                txtCliente.Text = "";
                txtSaldo.Text = "";
            }
        }

        protected void btnEliminar_Click(object sender, EventArgs e)
        {
            int Folio;
            try
            {
                var WS = new Servicio.ServicioWeb();
                Folio = Convert.ToInt32(txtFolio.Text);
                if (WS.Eliminar(Folio))
                {
                    lblStatus.Text = "Se ha eliminado el registro";
                    txtCliente.Text = "";
                    txtSaldo.Text = "";
                    txtFolio.Text = "";
                }
                else
                {
                    lblStatus.Text = "No se elimino el registro";
                }
            }
            catch (Exception ex)
            {
                lblStatus.Text = ex.Message;
            }
        }
    }
}