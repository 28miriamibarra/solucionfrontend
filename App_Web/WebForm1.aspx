﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="App_Web.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">
        .auto-style1 {
            height: 348px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div class="auto-style1">
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Label ID="Label1" runat="server" ForeColor="#660033" Text="App Web"></asp:Label>
            <br />
            <br />
&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Label ID="Label2" runat="server" Text="Folio"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:TextBox ID="txtFolio" runat="server" OnTextChanged="txtFolio_TextChanged"></asp:TextBox>
            &nbsp;
            <br />
            <br />
            <asp:Button ID="btnBuscar" runat="server" BackColor="#CC66FF" Height="37px" Text="Buscar" Width="125px" OnClick="btnBuscar_Click" />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Button ID="btnEliminar" runat="server" BackColor="#CC66FF" Height="38px" OnClick="btnEliminar_Click" Text="Eliminar" Width="126px" />
            <br />
            <br />
            <br />
            <asp:TextBox ID="txtCliente" runat="server"></asp:TextBox>
            <br />
            <asp:TextBox ID="txtSaldo" runat="server"></asp:TextBox>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Button ID="btnGuardar" runat="server" BackColor="#CC66FF" Height="36px" Text="Guardar" Width="125px" OnClick="btnGuardar_Click" />
            <br />
            <br />
            <br />
            <asp:Label ID="lblStatus" runat="server" Text="Status"></asp:Label>
            <br />
            <br />
            <br />
            <br />
        </div>
    </form>
</body>
</html>
