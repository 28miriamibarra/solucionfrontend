﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;
using System.Data;
using System;

namespace App_Android
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_main);
            var btnGuardar = FindViewById<Button>(Resource.Id.btnGuardar);
            var btnBuscar = FindViewById<Button>(Resource.Id.btnBuscar);
            var btnEliminar = FindViewById<Button>(Resource.Id.btnEliminar);
            var txtCliente = FindViewById<EditText>(Resource.Id.txtCliente);
            var txtSaldo = FindViewById<EditText>(Resource.Id.txtSaldo);
            var txtFolio = FindViewById<EditText>(Resource.Id.txtFolio);
            var lblStatus = FindViewById<TextView>(Resource.Id.lblStatus);
            btnGuardar.Click += delegate
            {
                int Folio;
                double Saldo;
                string Cliente;
                try
                {
                    var WS = new Servicio.ServicioWeb();
                    //Folio = int.Parse(txtFolio.Text);
                    Cliente = txtCliente.Text;
                    Saldo = double.Parse(txtSaldo.Text);
                    if (txtFolio.Text == "")
                    {
                        if (WS.Guardar(Cliente, Saldo))
                        {
                            lblStatus.Text = "Guardado Correctamente";
                            txtCliente.Text = "";
                            txtSaldo.Text = "";
                            txtFolio.Text = "";
                        }
                        else
                        {
                            lblStatus.Text = "No guardado";
                        }
                    }
                    else
                    {
                        Folio = int.Parse(txtFolio.Text);
                        if (WS.Actualizar(Folio, Cliente, Saldo))
                        {
                            lblStatus.Text = "Actualizado Correctamente";
                            txtCliente.Text = "";
                            txtSaldo.Text = "";
                            txtFolio.Text = "";
                        }
                        else
                        {
                            lblStatus.Text = "No se actualizo el registro";
                        }
                    }
                }
                catch (Exception ex)
                {
                    lblStatus.Text = ex.Message;
                }
                /*double Saldo;
                string Cliente;
                try
                {
                    var WS = new Servicio.ServicioWeb();
                    Cliente = txtCliente.Text;
                    Saldo = double.Parse(txtSaldo.Text);
                    if (WS.Guardar(Cliente, Saldo))
                    {
                        lblStatus.Text = "Guardado Correctamente";
                    }
                    else
                    {
                        lblStatus.Text = "No guardado";
                    }
                }
                catch (Exception ex)
                {

                    lblStatus.Text = ex.Message;
                }*/
            };
            btnBuscar.Click += delegate
            {
                int Folio;
                var Conjunto = new DataSet();
                DataRow Renglon;
                try
                {
                    lblStatus.Text = "";
                    Folio = int.Parse(txtFolio.Text);
                    var WS = new Servicio.ServicioWeb();
                    Conjunto = WS.BuscarRegistro(Folio);
                    Renglon = Conjunto.Tables["Datos"].Rows[0];
                    txtCliente.Text = Renglon["Cliente"].ToString();
                    txtSaldo.Text = Renglon["Saldo"].ToString();

                }
                catch (Exception ex)
                {
                    lblStatus.Text = "No existe el registro";
                    txtCliente.Text = "";
                    txtSaldo.Text = "";
                    //lblStatus.Text = ex.Message;

                }
            };
            btnEliminar.Click += delegate
            {
                int Folio;
                try
                {
                    var WS = new Servicio.ServicioWeb();
                    Folio = Convert.ToInt32(txtFolio.Text);
                    if (WS.Eliminar(Folio))
                    {
                        lblStatus.Text = "Se ha eliminado el registro";
                        txtCliente.Text = "";
                        txtSaldo.Text = "";
                        txtFolio.Text = "";
                    }
                    else
                    {
                        lblStatus.Text = "No se elimino el registro";
                    }
                }
                catch (Exception ex)
                {
                    lblStatus.Text = ex.Message;
                }
            };
        }
       
    }
}